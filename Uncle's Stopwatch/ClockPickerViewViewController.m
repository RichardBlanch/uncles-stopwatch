//
//  ClockPickerViewViewController.m
//  Uncle's Stopwatch
//
//  Created by Rich Blanchard on 12/25/15.
//  Copyright © 2015 Rich. All rights reserved.
//

#import "ClockPickerViewViewController.h"
#import "TimeForClocks.h"

@interface ClockPickerViewViewController () <UIPickerViewDataSource,UIPickerViewDelegate>

@end

@implementation ClockPickerViewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.seconds = [TimeForClocks getSeconds];
    self.minutes = [TimeForClocks getMinutes];
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    for (int i = 0; i < self.numberOfPickerViews; i++) {
        
        UILabel * minutes = [[UILabel alloc]initWithFrame:CGRectMake((screenWidth / 6), i*(screenHeight/4)+15, (screenWidth/2), 30)];
        [minutes setText:@"Minutes"];
        [self.view addSubview:minutes];
       
        UILabel * minutesLabel = [[UILabel alloc]initWithFrame:CGRectMake((screenWidth / 2) + (screenWidth / 7), i*(screenHeight/4)+15, (screenWidth/2), 30)];
        [minutesLabel setText:@"Seconds"];
        [self.view addSubview:minutesLabel];
        CGFloat difference = (screenWidth/6) - (screenWidth /2);
        
        UILabel * clockLabel = [[UILabel alloc]initWithFrame:CGRectMake(screenWidth / 2 - 25, i*(screenHeight/4)+15, 100, 30)];
        [clockLabel setText:[NSString stringWithFormat:@"Clock%d",(i+1)]];
        [clockLabel setTextColor:[UIColor grayColor]];
         [self.view addSubview:clockLabel];
         
        UIPickerView * pickerView = [[UIPickerView alloc]initWithFrame:CGRectMake(0, i*(screenHeight/4) + 25, screenWidth, (screenHeight / 4))];
        NSLog(@"The screen value is %f,",i*(screenHeight/4) + 25 + screenHeight/4);
       
        [self.view addSubview:pickerView];
        pickerView.delegate = self;
        pickerView.dataSource = self;
        self.addedMinutes = [[NSMutableArray alloc]init];
        self.addedSeconds = [[NSMutableArray alloc]init];
        
        
    }
   
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
}





- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if(component == 0) {
        return self.minutes[row];
    }
    else {
        return self.seconds[row];
    }
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (component == 0) {
        return self.minutes.count;
        
    }
    else {
        return self.seconds.count;
    }
    }

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2;
}
- (IBAction)GetClockTimes:(UIButton *)sender {
    for(UIView * view in self.view.subviews) {
        if([view isKindOfClass:[UIPickerView class]]) {
            UIPickerView * pickerView = (UIPickerView *)view;
            int row = [pickerView selectedRowInComponent:0];
            [self.addedMinutes insertObject:[self.minutes objectAtIndex:row] atIndex:0];
            int rowForSeconds = [pickerView selectedRowInComponent:1];
            [self.addedSeconds insertObject:[self.seconds objectAtIndex:rowForSeconds] atIndex:0];
        }
       
    }
  NSArray* reversedMinutes = [[self.addedMinutes reverseObjectEnumerator] allObjects];
     NSArray* reversedSeconds = [[self.addedSeconds reverseObjectEnumerator] allObjects];
    [self.delegate getTimesWithArrayOfMinutes:reversedMinutes andSeconds:reversedSeconds andTimesToRepeat:self.timesToRepeat];
    [self dismissViewControllerAnimated:YES completion:nil];
}
/*
-(void)addImageView:(float)lastYalue {
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    UIImageView * imageView = [[UIImageView alloc]initWithFrame:CGRectMake(screenWidth / 3, lastYalue,  150, 150)];
    imageView.image = [UIImage imageNamed:@"arnold"];
    [self.view addSubview:imageView];
}
 */






@end
