//
//  ViewController.h
//  Uncle's Stopwatch
//
//  Created by Rich Blanchard on 12/25/15.
//  Copyright © 2015 Rich. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *timerLabel;
@property (strong,nonatomic) NSTimer * timer;
@property  int counter;
@property (strong,nonatomic) NSString * stringFromCounter;
@property (strong,nonatomic) NSString * minutes;
@property  int clocksSelected;
@property  int amountOfTimesToRepeat;
@property (strong,nonatomic) NSMutableArray * minutesSelected;
@property (strong,nonatomic) NSMutableArray * secondsSelected;
@property  int currentClockTimeToRun;


@end

