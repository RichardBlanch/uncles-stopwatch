//
//  TimeForClocks.m
//  Uncle's Stopwatch
//
//  Created by Rich Blanchard on 12/25/15.
//  Copyright © 2015 Rich. All rights reserved.
//

#import "TimeForClocks.h"

@implementation TimeForClocks
+(NSArray *)getSeconds {
    return @[@"5",@"10",@"15",@"20",@"25",@"30",@"35",@"40",@"45",@"50",@"55"];
}
+(NSArray *)getMinutes {
    return @[@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10"];
}
@end
