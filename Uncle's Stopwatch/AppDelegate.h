//
//  AppDelegate.h
//  Uncle's Stopwatch
//
//  Created by Rich Blanchard on 12/25/15.
//  Copyright © 2015 Rich. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

