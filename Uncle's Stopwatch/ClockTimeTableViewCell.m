//
//  ClockTimeTableViewCell.m
//  Uncle's Stopwatch
//
//  Created by Rich Blanchard on 12/25/15.
//  Copyright © 2015 Rich. All rights reserved.
//

#import "ClockTimeTableViewCell.h"

@implementation ClockTimeTableViewCell

- (void)awakeFromNib {
   [self setBackgroundColor:[UIColor colorWithRed:(239./255.0) green:(239./255.0) blue:(239./255.0) alpha:1]];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

@end
