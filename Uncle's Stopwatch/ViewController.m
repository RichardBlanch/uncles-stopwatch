//
//  ViewController.m
//  Uncle's Stopwatch
//
//  Created by Rich Blanchard on 12/25/15.
//  Copyright © 2015 Rich. All rights reserved.
//

#import "ViewController.h"
#import "ClockPickerViewViewController.h"
#import "ClockTimeTableViewCell.h"


@interface ViewController () <giveClockTimes>


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView setBackgroundColor:[UIColor colorWithRed:(239./255.0) green:(239./255.0) blue:(239./255.0) alpha:1.0]];
    
    
    self.clocksSelected = 0;
   
    }
-(void)onTick {
    self.counter++;
    self.minutes = [self getMinutes];
   int seconds = self.counter % 60;
    NSString * stringForSeconds = [self getSecondsFromSingleDigit:seconds];
    self.stringFromCounter =  [NSString stringWithFormat:@"%@%@", self.minutes,stringForSeconds];
    self.timerLabel.text = self.stringFromCounter;

   
    
    if(self.counter < self.currentClockTimeToRun) {
        if(self.presentedViewController == nil) {
    [self callTimerToStart];
        }
        
    }
    else {
       
        [self updateTableView];
    }
    
   
}
-(NSString *)getMinutes
{
    int temp = self.counter / 60;
        NSString * minutes = [NSString stringWithFormat:@"%d:",temp];
        return minutes;
}
-(NSString *)getSecondsFromSingleDigit:(int)digit {
    switch (digit) {
        case 0:
            return @"00";
            break;
        case 1:
            return @"01";
        case 2:
            return @"02";
        case 3:
            return @"03";
        case 4:
            return @"04";
        case 5:
            return @"05";
        case 6:
            return @"06";
            break;
        case 7:
            return @"07";
        case 8:
            return @"08";
        case 9:
            return @"09";
    
        default:
            return [NSString stringWithFormat:@"%d",digit];
    }
}
- (IBAction)selectTimers:(UIBarButtonItem *)sender {
    [self removeAllItems];
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"Select Number Of Clocks" message:@"" preferredStyle:UIAlertControllerStyleAlert];
   
    UIAlertAction * one = [UIAlertAction actionWithTitle:@"1" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.clocksSelected = 1;
        [self selectAmountOfTimesForClocksToRepeat];
       
        
    }];
    UIAlertAction * two = [UIAlertAction actionWithTitle:@"2" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.clocksSelected = 2;
        [self selectAmountOfTimesForClocksToRepeat];
        
       
        
    }];
    UIAlertAction * three = [UIAlertAction actionWithTitle:@"3" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.clocksSelected = 3;
        [self selectAmountOfTimesForClocksToRepeat];
       
        
        
    }];
   
    [alert addAction:one];
    [alert addAction:two];
    [alert addAction:three];
    [self presentViewController:alert animated:YES completion:nil];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.secondsSelected.count;
}



- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
   
    
    ClockTimeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"tableView"];
    NSMutableString * concatonateMinutesAndSeconds = [NSMutableString stringWithFormat:@"%@:",self.minutesSelected[indexPath.row]];
    [concatonateMinutesAndSeconds appendString:self.secondsSelected[indexPath.row]];
    
    
    
    cell.clockLabel.text = [NSString stringWithFormat:@"Clock %ld",(long)(indexPath.row +1)];
    cell.timeSelectedLabel.text = concatonateMinutesAndSeconds;
    return cell;
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    ClockPickerViewViewController * vc = (ClockPickerViewViewController *)segue.destinationViewController;
    vc.numberOfPickerViews = self.clocksSelected;
    vc.delegate = self;
    vc.timesToRepeat = self.amountOfTimesToRepeat;
}
-(void)getTimesWithArrayOfMinutes:(NSArray *)minutes andSeconds:(NSArray *)getSeconds andTimesToRepeat:(int)timesToRepeat {
    self.minutesSelected = [[NSMutableArray alloc]init];
     self.secondsSelected = [[NSMutableArray alloc]init];
    for(int i = 0; i<timesToRepeat; i++) {
        for(int j=0; j<minutes.count; j++) {
            [self.minutesSelected insertObject:minutes[j] atIndex:0];
            [self.secondsSelected insertObject:getSeconds[j] atIndex:0];
        }
    }
    NSLog(@"minutesSelected is %@",self.minutesSelected);
    [self.tableView reloadData];
    [self callTimerToStart];
    

}
-(void)callTimerToStart {
    
   
    if(self.minutesSelected > 0) {
    self.currentClockTimeToRun = [self convertArrayTimeToMinutes:self.minutesSelected andSeconds:self.secondsSelected];

    
    self.timer = [[NSTimer alloc]init]; [NSTimer scheduledTimerWithTimeInterval: 1.0
                                                                         target: self
                                                                       selector:@selector(onTick)
                                                                       userInfo: nil repeats:NO];}
    
   
    
}
-(int)convertArrayTimeToMinutes:(NSArray *)minutes andSeconds:(NSArray *)seconds {
    
    NSString * minutesStringValue = (NSString*)minutes[0];
    int getMinutes = [minutesStringValue intValue];
    
    NSString * secondsStringValue = (NSString*)seconds[0];
    int getSeconds = [secondsStringValue intValue];
    
    int convertMinutesToSeconds = (getMinutes * 60);
    
    int secondsAddedToMinutes = (getSeconds + convertMinutesToSeconds);
    NSLog(@"The amount of seconds are %d",secondsAddedToMinutes);
    
    return secondsAddedToMinutes;
}
-(void)removeFirstItemsOfEachArray {
    [self.minutesSelected removeObjectAtIndex:0];
    [self.secondsSelected removeObjectAtIndex:0];
}
   -(void)removeAllItems {
       [self.minutesSelected removeAllObjects];
       [self.secondsSelected removeAllObjects];
       self.counter = 0;
       self.timerLabel.text = 0;
   }
-(void)updateTableView {
    self.counter = 0;
    self.timerLabel.text = 0;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
  
    [self.tableView beginUpdates];
    [self removeFirstItemsOfEachArray];
    [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    [self.tableView endUpdates];
    if(self.minutesSelected.count > 0) {
    [self callTimerToStart];
    }
}
-(void)selectAmountOfTimesForClocksToRepeat {
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"Select Amount Of times for Clocks To Repeat" message:@"" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction * one = [UIAlertAction actionWithTitle:@"1" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.amountOfTimesToRepeat = 1;
         [self performSegueWithIdentifier:@"pickerViews" sender:self];
       
        
    }];
    UIAlertAction * two = [UIAlertAction actionWithTitle:@"2" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.amountOfTimesToRepeat = 2;
         [self performSegueWithIdentifier:@"pickerViews" sender:self];

        
        
    }];
    UIAlertAction * three = [UIAlertAction actionWithTitle:@"3" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.amountOfTimesToRepeat = 3;
         [self performSegueWithIdentifier:@"pickerViews" sender:self];
        
        
        
    }];
    UIAlertAction * four = [UIAlertAction actionWithTitle:@"4" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.amountOfTimesToRepeat = 4;
         [self performSegueWithIdentifier:@"pickerViews" sender:self];
        
        
    }];
    [alert addAction:one];
    [alert addAction:two];
    [alert addAction:three];
    [alert addAction:four];
    [self presentViewController:alert animated:YES completion:nil];
}




@end
