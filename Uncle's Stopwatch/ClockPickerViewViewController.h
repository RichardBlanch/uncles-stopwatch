//
//  ClockPickerViewViewController.h
//  Uncle's Stopwatch
//
//  Created by Rich Blanchard on 12/25/15.
//  Copyright © 2015 Rich. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol giveClockTimes
-(void)getTimesWithArrayOfMinutes:(NSArray *)minutes andSeconds:(NSArray *)getSeconds andTimesToRepeat:(int)timesToRepeat;
@end

@interface ClockPickerViewViewController : UIViewController
@property int numberOfPickerViews;
@property int timesToRepeat;
@property (nonatomic,strong) NSArray * seconds;
@property (nonatomic,strong) NSArray * minutes;
@property (nonatomic,strong) NSMutableArray * addedMinutes;
@property (nonatomic,strong) NSMutableArray * addedSeconds;
@property (nonatomic,weak) id<giveClockTimes> delegate;


@end
