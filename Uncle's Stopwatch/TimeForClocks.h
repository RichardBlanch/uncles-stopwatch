//
//  TimeForClocks.h
//  Uncle's Stopwatch
//
//  Created by Rich Blanchard on 12/25/15.
//  Copyright © 2015 Rich. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TimeForClocks : NSObject
+(NSArray *)getMinutes;
+(NSArray *)getSeconds;

@end
